import React ,{useState} from 'react';
import Sidebar from "./component/Sidebar/Sidebar";
import Academics from "./pages/Academics/Academics";
import Career from "./pages/Career/Career";
import Home from "./pages/Home/Home";
import Achievements from "./pages/Achievements/Achievements";
import {Route} from 'react-router-dom';
///import Pill from "./component/Pill/Pill";
function App() {

  //   const [page, setPage] = useState<string>('');
  // const navigateTo = (url: string) => setPage(url);


  return (
    <>
       <Sidebar/>
      <main>

        <Route path="/" exact={true}>
          <Home/>
        </Route>
        <Route path="/Achievements" exact={true}>
          <Achievements/>
        </Route>
        <Route path="/Career" exact={true}>
          <Career/>
        </Route>
        <Route path="/Academics" exact={true}>
          <Academics/>
        </Route>
        {/* {
          page === '' && <Home />
        }


        
        {
          page === 'Achievements' && <Achievements />
        }

        {
            page==='Career' && <Career/>
        }

        {
            page==='Academics' && <Academics/>
        } */}

      </main> 
    </>
  );
}

export default App;
