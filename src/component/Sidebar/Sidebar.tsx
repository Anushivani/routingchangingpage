 import Pill from '../Pill/Pill';
 import styles from './Sidebar.module.scss';
 import { IProps } from './Sidebar.types';
 import {Link} from "react-router-dom";

const Sidebar = () => {
    return (
        <header className={styles.sidebar}>
            <nav>
                <Link to="/">
                <Pill text="Home"/>
                </Link>
               <Link to="/Achievements">
               <Pill text="Achievements"/>
               </Link>
               <Link to="/Career">
               <Pill text="Career"/>
               </Link>
                 <Link to="/Career">
                 <Pill text="Career"/>
                 </Link>
            </nav>
        </header>
    );
};

export default Sidebar;
